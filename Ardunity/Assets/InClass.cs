﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class InClass : MonoBehaviour
{
    public GameObject gameObject1;
    public GameObject gameObject2;

    SerialPort arduino = new SerialPort("COM8", 9600);

    // Start is called before the first frame update
    void Start()
    {
        arduino.Open();
        arduino.ReadTimeout = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(arduino.IsOpen)
        {
            try
            {
                string arduinoString = arduino.ReadLine();
                if (arduinoString != "0")
                {

                    string[] valueString = arduinoString.Split(',');
                    int potValue = int.Parse(valueString[0]);
                    pot(potValue);

                    // button press
                    if (valueString[1] == "1")
                    {
                        button();
                    }
                }
            }
            catch (System.Exception)
            {
            }
        }
    }

    void button()
    {
        gameObject1.transform.position = new Vector3(gameObject1.transform.position.x, gameObject1.transform.position.y + 1.0f, gameObject1.transform.position.z);
    }

    void pot(int temp)
    {
        //Debug.Log(temp);
        gameObject2.transform.position = new Vector3(gameObject2.transform.position.x, temp, gameObject2.transform.position.z);
    }
}
